#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define OFFSET 2

struct REGISTER {
    char URL[100];
    char login[100];
    char senha[100];
};
typedef struct REGISTER REGISTER;

#define OFFSET 2

void encode(char text[100]) {
    for(int i=0; i<strlen(text); i++) {
        text[i] += OFFSET;
    }
}

void decode(char text[100]) {
    for(int i=0; i<strlen(text); i++) {
        text[i] -= OFFSET;
    }
}

int isValidOperation(int func) {
    return func == 1 || func == 2 || func == 3;
}

int validateAuth(char usuarioLogin[100], char sistemaLogin[100], char usuarioSenha[100], char sistemaSenha[100]) {
    return strcmp(usuarioLogin, sistemaLogin) == 0 && strcmp(usuarioSenha, sistemaSenha) == 0;
}

void cadastraUsuario() {
    // Codigo do cadastro
    char usuarioLogin[100];
    char usuarioSenha[100];
    FILE *manager = fopen("manager.bin", "w");

    printf("Voce esta prestes a criar um novo usuario.\n");
    printf("Se algum conteudo ja existia de um usuario anterior, esse conteudo foi DELETADO.\n");
    printf(">> Login: ");
    scanf("%s", usuarioLogin);
    printf(">> Senha: ");
    scanf("%s", usuarioSenha);

    encode(usuarioLogin);
    encode(usuarioSenha);

    fprintf(manager, "%s\n", usuarioLogin);
    fprintf(manager, "%s\n", usuarioSenha);
    fclose(manager);
}

void autenticaUsuario(char *usuarioLogin, char *usuarioSenha) {
    // Codigo de login
    char sistemaLogin[100];
    char sistemaSenha[100];

    FILE* manager = fopen("manager.bin", "r");
    fscanf(manager, "%s", sistemaLogin);
    fscanf(manager, "%s", sistemaSenha);

    decode(sistemaLogin);
    decode(sistemaSenha);

    printf(">> Login: ");
    scanf("%s", usuarioLogin);
    printf(">> Senha: ");
    scanf("%s", usuarioSenha);

    while (!validateAuth(usuarioLogin, sistemaLogin, usuarioSenha, sistemaSenha)) {
        printf("Login e/ou senha incorretos. Digite novamente.\n\n");
        printf(">> Login: ");
        scanf("%s", usuarioLogin);
        printf(">> Senha: ");
        scanf("%s", usuarioSenha);
    }

    fclose(manager);
}

void cadastraRegistro() {
    REGISTER novoReg;
    FILE *manager = fopen("manager.bin", "a");

    printf("Voce esta prestes a cadastrar uma nova senha.\n");
    printf(">> URL: ");
    scanf("%s", novoReg.URL);
    printf(">> Login: ");
    scanf("%s", novoReg.login);
    printf(">> Senha: ");
    scanf("%s", novoReg.senha);

    encode(novoReg.URL);
    encode(novoReg.login);
    encode(novoReg.senha);

    fprintf(manager, "%s\n", novoReg.URL);
    fprintf(manager, "%s\n", novoReg.login);
    fprintf(manager, "%s\n", novoReg.senha);

    fclose(manager);
}

void encontraRegistro() {
    REGISTER registros[100];
    FILE *manager = fopen("manager.bin", "r");
    char url[100];
    char auth[100];
    int idReg = 0;
    int urlEncontrada = -1;

    //As duas primeiras linhas sao para autenticacao de usuario, logo sao ignoradas
    fscanf(manager, "%s", auth);
    fscanf(manager, "%s", auth);

    // Le todos os registros e armazena no array
    while(1) {
        if(fscanf(manager, "%s", registros[idReg].URL) == EOF) {
            break;
        } else {
            decode(registros[idReg].URL);
        }

        if(fscanf(manager, "%s", registros[idReg].login) == EOF) {
            break;
        } else {
            decode(registros[idReg].login);
        }

        if(fscanf(manager, "%s", registros[idReg].senha) == EOF) {
            break;
        } else {
            decode(registros[idReg].senha);
        }
                
        idReg++;
    }
            
    printf(">> URL: ");
    scanf("%s", url);

    for(int i=0; i<idReg; i++) {
        if(strcmp(url, registros[i].URL) == 0) {
            urlEncontrada = i;
            break;
        }
    }

    if(urlEncontrada == -1) {
        printf("Essa URL nao foi encontrada. Tente novamente.\n\n");
    } else {
        printf("URL %s encontrada.\n\n", registros[urlEncontrada].URL);
        printf("---************---\n");
        printf(">> Login: %s\n", registros[urlEncontrada].login);
        printf(">> Senha: %s\n", registros[urlEncontrada].senha);
        printf("---************---\n\n\n");
    }

    fclose(manager);
}

int encontraUserOperacao() {
    int userOp = 0;

    printf("Escolha o numero de sua operacao abaixo:\n");
    printf("------------------\n");
    printf("(1) Cadastrar nova senha\n(2) Checar Senha\n(3) Encerrar sessao\n");
    printf("------------------\n");
    printf(">> Operacao: ");
    scanf("%d", &userOp);

    while(!isValidOperation(userOp)) {
        printf("Operacao invalida! Digite uma operacao valida (1, 2 ou 3). \n");
        printf(">> Operacao: ");
        scanf("%d", &userOp);        
    }

    return userOp;
}

int encontraOperacao() {
    int op;

    printf("Bem-vindo ao (SCLS) Sistema de Cadastro e Login de Senhas.\nO que deseja fazer?\n\n");
    printf("Escolha o numero de sua operacao abaixo:\n");
    printf("------------------\n");
    printf("(1) Cadastro\n(2) Login\n(3) Encerrar sessao\n");
    printf("------------------\n");
    printf(">> Operacao: ");
    scanf("%d", &op);

    while(!isValidOperation(op)) {
        printf("Operacao invalida! Digite uma operacao valida (1, 2 ou 3). \n");
        printf(">> Operacao: ");
        scanf("%d", &op);
    }

    return op;
}

void executaUserOperacao(int userOp) {
    if(userOp == 1) {
        cadastraRegistro();
    } else if(userOp == 2) {
        encontraRegistro();
    } 
}

void executaOperacao(int op) {
    char usuarioLogin[100];
    char usuarioSenha[100];
    
    if(op == 1) {
        cadastraUsuario();
    } else if(op == 2) {
        int userOp = 0;
        autenticaUsuario(usuarioLogin, usuarioSenha);
        printf("Seja bem-vindo, %s!\n", usuarioLogin);
        while(userOp != 3) {
            userOp = encontraUserOperacao();
            executaUserOperacao(userOp);
        }
    }
}

void encerraSessao() {
    printf("\n\nSessao encerrada!\nObrigado por usar nosso servico!\n");
}

int main() {
    int op = 0;
    while(op != 3) {
        op = encontraOperacao();
        executaOperacao(op);
    }
    encerraSessao();

    return 0;
}